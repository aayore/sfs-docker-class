# Introduction to Docker

!

# Introduction

- What is Docker?
- Why use Docker?
- How is a container different from a VM?
- Docker and 12-Factor design

<!-- No persistent storage! -->

!

## Installation

- Don't need to use a VM
- Native client for Linux, Windows, MacOS
- [Downloads](https://store.docker.com/search?type=edition&offering=community)

[Lab](https://gitlab.com/aayore/sfs-docker-class/blob/master/lab_installation.md)

<!-- Lab: lab_installation.md -->

!

## Running a Container

- `docker run ...`
- `docker help run`
- Get stuff from [Docker Hub](https://hub.docker.com/)
- `docker search`
- Be careful!

[Lab](https://gitlab.com/aayore/sfs-docker-class/blob/master/lab_running.md)

<!-- Lab: lab_running.md -->
<!-- Discuss interactive vs. detached -->

!

## Exposing Ports

- Containers are inaccessible by default

[Lab](https://gitlab.com/aayore/sfs-docker-class/blob/master/lab_ports.md)

<!-- Lab: lab_ports.md -->

!

## Terms

- Registry vs. Repo
- Image vs. Container
- Tags
- Push and Pull

<!-- No lab. -->

!

## Passing Config

- Environment variables
- Environment files

[Lab](https://gitlab.com/aayore/sfs-docker-class/blob/master/lab_configs.md)

<!-- Lab: lab_configs.md -->

!

## Stateful Data (or Configs)

- Don't count on container data
- Mounting volumes

[Lab](https://gitlab.com/aayore/sfs-docker-class/blob/master/lab_state.md)

<!-- Lab: lab_state.md -->

!

## Examining the Unknown

- An image you want to use
- A running container
- `docker pull ...`
- `docker inspect ...`

[Lab](https://gitlab.com/aayore/sfs-docker-class/blob/master/lab_inspect.md)

<!-- Lab: lab_inspect.md -->

!

## Stopping

- `docker stop`
- `docker kill`
- `docker rm --force`

[Lab](https://gitlab.com/aayore/sfs-docker-class/blob/master/lab_stop.md)

!

## Cleaning Up

- `docker ps --all`
- `docker images`
- `docker {object} prune` : container, image, system
- `docker rm` and `docker rmi`

[Lab](https://gitlab.com/aayore/sfs-docker-class/blob/master/lab_cleanup.md)

<!-- Lab: Examine what we have running, clean it up -->

!

## Building Your Own

- Dockerfile
- Context
- `docker build <context>`

[Lab 1](https://gitlab.com/aayore/sfs-docker-class/blob/master/lab_nethack.md)

[Lab 2](https://gitlab.com/aayore/sfs-docker-class/blob/master/lab_webserver.md)

<!-- Lab: Create our own Dockerfile, build it -->

!

## Pushing to DockerHub

- Sharing with others
- Facilitating deployments
- Moving an image

[Lab](https://gitlab.com/aayore/sfs-docker-class/blob/master/lab_push.md)

<!-- Lab: Push our image -->

!

## Artifacts

- Commit hash
- Software package
- Container

<!-- Lab: ??? -->

!

## Extras: If we have time...

- prototyping
- `docker network`
- `docker compose`

!

## The End

- Thank you!

## Questions?

- What should I put in the next Docker class?
