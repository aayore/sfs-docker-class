> From: ettiejr@ettiepy.com<br>
> Subject: Devs need the web server...
>
>   Great job on the web server!  We've got some remote web designers who need access to the image you created.  Can you push it up to DockerHub so they can grab their own copy?
>
> You're the best!
>
> EJ

---

1. Log in to [Docker Hub](https://hub.docker.com) - Web
2. `docker login` to log in to Docker Hub at the command line
3. Create the repository using the web interface
4. Tag your image with `docker tag webserver <docker_hub_username>/webserver`
5. Push the image with `docker push <docker_hub_username>/webserver`
6. Pull and run your partner's image!
