> From: ettiejr@ettiepy.com<br>
> Subject: Managing Configurations
>
>   We do our best to practice [Twelve Factor](https://12factor.net) application design.  Docker helps us with the third factor, separating our config from our app.  Take some time today and figure out how to pass environment variables and environment files into a Docker container.
>
> -EJ
>
> P.S. - This can be complicated by shell behavior.  Instead of the command you used in an earlier lab, start with this: `docker run ubuntu /bin/bash -c 'echo hello world'`  That will help get the command into the container correctly.  It's a weird shell-ism that you probably won't have to worry about too much.

---

1. Make sure the command EJ suggested runs for you.
2. What happens if you run `docker run ubuntu /bin/bash -c 'echo $MYVAR'`?
3. Try using the `--env MYVAR="hello world"` option to pass a variable to your container.

1. Create a simple file called myEnvFile that has one line: `ANOTHERVAR="hello mars"`
2. Use the `--env-file` option with `docker run ubuntu /bin/bash -c 'echo $ANOTHERVAR'`
