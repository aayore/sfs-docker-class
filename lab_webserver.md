> From: ettiejr@ettiepy.com<br>
> Subject: A Simple Web Server
>
>   Now that you managed to build yourself a Nethack image, you should be able to tackle a simple web server.  We're just looking for a prototype that you can stand up pretty quickly.
>
> Thanks!
>
> EJ

---

Run a simple web server:

1. Use `docker search HTTP` to find the Apache HTTP Server.
2. Use `docker pull` and `docker inspect` the image - figure out which port(s) you'll need to publish.
3. Run a container with `docker run --detach --publish 8080:XX --name mywebserver <image_name>`
4. Navigate to [http://localhost:8080](http://localhost:8080) in a web browser
5. Stop your container with `docker stop mywebserver`

Add some basic content:

1. Make a new directory
2. Put your favorite joke in a file called `index.html`
3. Create a Dockerfile for the web server
4. Put a `FROM` line and a `COPY` line in your Dockerfile:
    - The `FROM` line should include the image you found above
    - `COPY index.html /usr/local/apache2/htdocs`
5. Build with `docker build --tag webserver .`
6. Run a container like you did above, but using your new image:
    - `docker run --detach --publish 8080:XX --name mywebserver webserver`
7. We forgot to `docker stop mywebserver`!
8. We also need to `docker rm mywebserver`!
9. Now try step 6 again...
10. And open up [http://localhost:8080](http://localhost:8080)
